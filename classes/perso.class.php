<?php

class Personnage {
    private $nom ;
    private $type ;
    private $genre ;
    private $taille ;
    private $intel ;
    private $force ;
    private $dexterite ;
    private $apparence ;
    private $vitesse ;
    private $attaque ;
    private $defense ;
 
    protected const AVATAR = "images/avatar.jpg" ;

    public function __destruct() {
        //ici on sauvegarde des données, etc... avant de destroy l'objet
    } 

    public function __construct($n, $g) {
        $this->nom = $n ;
        $this->genre = $g ;
    }
    //getter
    public function get_nom() {
        return $this->nom ;
    }

    //setter
    public function set_nom($n) {
        $this->nom = $n ;
    }
}

class Magicien extends Personnage {
    protected const AVATAR = "images/magicien/avatar.jpg" ;

    public function get_nom() {
        return "***" . $this->nom."***" ;
    }

    public static function get_avatar() {
        return SELF::AVATAR ;
    }
}

class Animal {
    private $nom ;
    private $type ;
    private $genre ;
    private $taille ;
    private $habitat ; //à vérifier
    private $apparence ;
    private $eteint ; //true or false
    private $age ;

    //getter
    public function get_nom() {
        return $this->nom ;
    }

    //setter
    public function set_nom($n) {
        $this->nom = $n ;
    }
    
    public function __construct($n, $tp, $h) {
        $this->nom = $n ;
        $this->type = $tp ;
        $this->habitat = $h ;
    }
}

?>