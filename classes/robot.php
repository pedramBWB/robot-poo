<?php 

class Robot {
    private $name;
    private $x;
    private $y;
    private $orientation;
    private $size ;
    private $couleur;
    private $action;
    private $autonomie;
    private $maxspeed ;
    private $arena ;

    public function __construct($n, $x, $y, $o, $s, $c, $a, $au, $m, $ar) {
        $this->name = $n;
        $this->x = $x;
        $this->y = $y;
        $this->orientation = $o;
        $this->size = $s;
        $this->couleur = $c;
        $this->action = $a;
        $this->autonomie = $au;
        $this->maxspeed = $m;
        $this->arena = $ar;
        array_push($this->arena->bots, $this) ;//bots = array
    }

    public function get_position() {
        return [$this->x , $this->y, $this->orientation] ;
    }

    public function move($longueurx, $longueury) {
        $this->x += $longueurx ;
        $this->y += $longueury ;
        $this->arena->get_bot_pos($bot1) ;

    }

    public function turn($orientation) {
        $this->orientation = $orientation ;
        //c'est compliqué, pas mtn
    }

    public function exec($action) {

    }

    public function speech($action) {

    }

}

class Arena {
    private $name ;
    private $sizex ; // en pixel ?
    private $sizey ;
    private $bots = [] ;

    public function __construct($n, $sx, $sy){
        $this->name = $n ;
        $this->sizex = $sx ;
        $this->sizey = $sy ;
    }

    public function get_bot_pos($bot) {
        $this->$bot = $bot ;
    }
}

$arena1 = new Arena("KEKW Arena", 50, 50); //supposé être une div 

$bot1 = new Robot("Philippe", 0, 0, 45, "small", "blue", "collect", 2, 2, $arena1);
//echo $bot1->get_position(); //contrôle d'affichage
echo "<pre>" ;
/*
var_dump($bot1->get_position()) ;
$bot1->turn(90);
$bot1->move(50, 50);
var_dump($bot1->get_position()) ;
echo "nice"; */

$bot2 = new Robot("Michel", 50, 50, 225, "small", "red", "run", 2, 100, $arena1);
$bot1->move(50, 50);

var_dump($arena1)
?>